﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using ReportManager.Interface.Models;

namespace ReportManager.Models
{
    public class Configuration : IConfiguration
    {
        [JsonRequired]
        [JsonProperty(@"bh2")]
        public float BaseH2Limit { get; set; }

        [JsonRequired]
        [JsonProperty(@"h2")]
        public float H2Limit { get; set; }

        [JsonRequired]
        [JsonProperty(@"ch4")]
        public float CH4Limit { get; set; }

        [JsonRequired]
        [JsonProperty(@"ch4h2")]
        public float CH4H2Limit { get; set; }

        [JsonRequired]
        [JsonProperty(@"intervals")]
        public ICollection<int> DefaultMinutes { get; set; }

        public Configuration()
        {
            BaseH2Limit = 10.0f;
            H2Limit = 20.0f;
            CH4Limit = 3.0f;
            CH4H2Limit = 15.0f;
            DefaultMinutes = new ObservableCollection<int>();
        }

        public Configuration(IConfiguration other)
        {
            BaseH2Limit = other.BaseH2Limit;
            H2Limit = other.H2Limit;
            CH4Limit = other.CH4Limit;
            CH4H2Limit = other.CH4H2Limit;
            DefaultMinutes = other.DefaultMinutes;
        }
    }
}