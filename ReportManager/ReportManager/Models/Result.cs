﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using ReportManager.Interface.Models;
using ReportManager.ViewModels;

namespace ReportManager.Models
{
    public class Result : BaseViewModel, IResult
    { 
        [JsonProperty("minute")]
        public int Minute { get; set; }

        [JsonProperty("time")]
        public DateTime? Time { get; set; }

        [JsonIgnore]
        public DateTime? StartTime { get; set; }

        [JsonProperty("h2")]
        public float H2Value { get; set; }

        [JsonProperty("ch4")]
        public float CH4Value { get; set; }

        [JsonProperty("co2")]
        public bool CO2 { get; set; }

        public Result()
        {
            Time = null;
            StartTime = null;
            Minute = 0;
            H2Value = 0;
            CH4Value = 0;
            CO2 = true;
        }

        public Result(IResult other)
        {
            if (other == null) return;
            StartTime = other.StartTime;
            Minute = other.Minute + 20;
        }

        private void CalculateTime()
        {
            if (StartTime == null) return;
            Time = (StartTime is DateTime ? (DateTime) StartTime : new DateTime()).AddMinutes(Minute);
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);
            if (args.PropertyName == nameof(StartTime) || args.PropertyName == nameof(Minute))
            {
                CalculateTime();
            }
        }
    }
}
