﻿using System;
using Newtonsoft.Json;
using ReportManager.Interface.Models;

namespace ReportManager.Models
{
    public class Patient : IPatient
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("surname")]
        public string Surname { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonIgnore]
        public DateTime Birthday { get; set; }

        public Patient()
        {
            Birthday = DateTime.Today;
        }
    }
}
