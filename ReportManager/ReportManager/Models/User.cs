﻿using ReportManager.Interface.Models;

namespace ReportManager.Models
{
    public class User : IUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
