﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Microsoft.Practices.ObjectBuilder2;
using Newtonsoft.Json;
using Prism.Commands;
using ReportManager.Helpers;
using ReportManager.Interface.Models;
using ReportManager.ViewModels;

namespace ReportManager.Models
{
    [JsonObject("analysis")]
    public class Analysis : BaseViewModel, IAnalysis
    {
        [JsonIgnore]
        public CollectionViewSource ViewSource { get; set; }

        [JsonProperty("results")]
        public ObservableCollection<IResult> Results { get; set; }

        [JsonIgnore]
        public Result CurrentItem { get; set; }

        [JsonProperty("patient")]
        public IPatient Patient { get; set; }

        [JsonProperty("supervisor")]
        public IUser Supervisor { get; set; }

        [JsonProperty("datestarted")]
        public DateTime DateStarted { get; set; }

        [JsonIgnore]
        public ICommand RefreshResultsCommand => new DelegateCommand(RefreshResults);

        public Analysis()
        {
            DateStarted = DateTime.Now;
            Patient = new Patient();
            Supervisor = new User();
            Results = new ObservableCollection<IResult>();
        }

        public Analysis(IConfiguration config)
        {
            DateStarted = DateTime.Now;
            Patient = new Patient();
            Supervisor = new User();
            InitResults(config);
            ViewSource = new CollectionViewSource { Source = this.Results };
            ViewSource.SortDescriptions.Add(new SortDescription(nameof(Result.Minute), ListSortDirection.Ascending));
            ViewSource.IsLiveSortingRequested = true;
        }

        private void InitResults(IConfiguration config)
        {
            var resultsConfig = config.DefaultMinutes;
            Results = new ObservableCollection<IResult>();

            foreach (var minute in resultsConfig)
            {
                Results.Add(new Result
                {
                    Minute = minute,
                    StartTime = DateStarted
                });
            }
        }

        public void RefreshResults()
        {
            try
            {
                ViewSource?.View.Refresh();
            }
            catch
            {
                
            }
        }

        public void RemoveSelected()
        {
            if (CurrentItem != null)
            {
                Results.Remove(CurrentItem);
            }
        }

        public void AddNew()
        {
            Results.Add(new Result(Results.LastOrDefault()){StartTime = DateStarted});
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);

            if (args.PropertyName == nameof(DateStarted))
            {
                Results?.ForEach(res => res.StartTime = DateStarted); 
                RaisePropertyChanged(nameof(Results));
            }

            if (args.PropertyName == nameof(Results))
            {
                RefreshResults();
            }
        }
    }
}
