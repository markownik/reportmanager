﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using ReportManager.Helpers;
using ReportManager.Interface.Models;
using ReportManager.Interface.Services;

namespace ReportManager.Services
{
    public class PdfService : IPdfService
    {
        private readonly IHtmlService _htmlService;

        private string _html;

        public PdfService(IHtmlService htmlService)
        {
            _htmlService = htmlService;
        }

        public void Init(IConfiguration config)
        {
            _htmlService.Setup(config);
            Directory.CreateDirectory(AppSettings.TempFolderBase);
        }

        public void Load(IAnalysis analysis)
        {
            _html = _htmlService.Generate(analysis);
        }

        public void SaveToFile(string filename)
        {
            try
            {
                File.WriteAllText($"{AppSettings.TempFolderBase}temp.html", _html);

                var p = new Process();

                var backup = CultureInfo.CurrentCulture;
                CultureInfo.CurrentCulture = new CultureInfo("en-US");

                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.WorkingDirectory = AppSettings.TempFolderBase;
                p.StartInfo.FileName = AppSettings.WkPath;
                p.StartInfo.Arguments =
                    $"--javascript-delay 200 --dpi 96 --zoom {AppSettings.DpiScale.DpiScaleY:F2} --margin-left 0.9cm --margin-right 0.9cm --margin-top 0.9cm --margin-bottom 0.9cm --title Raport \"{AppSettings.TempFolderBase}temp.html\" \"{AppSettings.TempFolderBase}temp\"";
#if DEBUG
                Console.WriteLine($"Scale: {AppSettings.DpiScale.DpiScaleY:F2}");
#endif
                CultureInfo.CurrentCulture = backup;

                p.Start();

                var error = p.StandardError.ReadToEnd();

                Console.Write(p.StandardOutput.ReadToEnd());
                if(!string.IsNullOrEmpty(error))
                    Console.Write($"Completed with errors:\n {error}\n");

                p.WaitForExit();

                File.Copy($"{AppSettings.TempFolderBase}temp", $"{filename}");
#if !DEBUG
                Directory.Delete(AppSettings.TempFolderBase, true);
#endif
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                    new ThreadStart(delegate { Mouse.OverrideCursor = Cursors.Arrow; }));
            }
        }
    }
}