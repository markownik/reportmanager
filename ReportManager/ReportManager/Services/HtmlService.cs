﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using HtmlAgilityPack;
using Microsoft.Practices.ObjectBuilder2;
using Newtonsoft.Json;
using ReportManager.Interface.Models;
using ReportManager.Interface.Services;
using ReportManager.Properties;
using ReportManager.Helpers;
using ReportManager.Models;

namespace ReportManager.Services
{
    public class HtmlService : IHtmlService
    {
        private IConfiguration _config;

        private IAnalysis _analysis;

        private Report _report;

        public void Setup(IConfiguration configuration)
        {
            _config = configuration;
        }

        public string Generate(IAnalysis analysis)
        {
            _analysis = analysis;
            _report = new Report(analysis, _config);

            string temp;

            var doc = new HtmlDocument();
            doc.LoadHtml(Resources.template);

            doc.DocumentNode.Descendants("link")
                .Single(d => d.Attributes.Contains("rel") && d.Attributes["rel"].Value.Contains("stylesheet") && 
                             d.Attributes.Contains("href") && d.Attributes["href"].Value.Contains("fonts"))
                .Remove();

            var style = doc.CreateElement("style");
            style.AppendChild(doc.CreateTextNode(Resources.fonts));
            doc.DocumentNode.SelectSingleNode("//head").InsertBefore(style, doc.DocumentNode.Descendants("style").First());

            var script = doc.DocumentNode.Descendants("script")
                .Single(d => d.Attributes.Contains("src") && d.Attributes["src"].Value.Contains("jquery"));
            script.Attributes.Remove("src");
            script.AppendChild(doc.CreateTextNode(Resources.jquery));

            script = doc.DocumentNode.Descendants("script")
                .Single(d => d.Attributes.Contains("src") && d.Attributes["src"].Value.Contains("chart"));
            script.Attributes.Remove("src");
            script.AppendChild(doc.CreateTextNode(Resources.chart));

            //var temp_logo = $"{AppSettings.TempFile}.png";

            //File.WriteAllBytes(temp_logo, Convert.FromBase64String(Resources.logo.Base64Encoded(ImageFormat.Png)));

            //doc.DocumentNode.Descendants("img")
            //    .Single(d => d.HasClass("logo"))
            //    .SetAttributeValue("src", $"file:///{temp_logo.Replace('\\','/').Replace("c/","c:/")}");

            if (!string.IsNullOrWhiteSpace(_analysis.Patient.Name) &&
                !string.IsNullOrWhiteSpace(_analysis.Patient.Surname))
            {
               doc.DocumentNode.Descendants("p")
                .Where(p => p.HasClass("patient")).ForEach(p =>
                {
                    p.AddClass("visible");
                    temp = p.InnerText;
                    temp = string.Format(temp, _analysis.Patient.Name, _analysis.Patient.Surname);
                    p.RemoveAllChildren();
                    p.AppendChild(doc.CreateTextNode(temp));
                });
            }


            if (!string.IsNullOrWhiteSpace(_analysis.Supervisor.Name) &&
                !string.IsNullOrWhiteSpace(_analysis.Supervisor.Surname))
            {
                doc.DocumentNode.Descendants("p")
                    .Where(p => p.HasClass("doctor")).ForEach(p =>
                    {
                        p.AddClass("visible");
                        temp = p.InnerText;
                        temp = string.Format(temp, _analysis.Supervisor.Name, _analysis.Supervisor.Surname);
                        p.RemoveAllChildren();
                        p.AppendChild(doc.CreateTextNode(temp));
                    });
            }
            
            //set right amount of time in beggining of the report.
            doc.DocumentNode.Descendants("p")
                .Where(p => p.HasClass("test-length"))
                .ForEach(p =>
                {
                    temp = p.InnerText;
                    temp = string.Format(temp, analysis.Results.OrderBy(x => x.Minute).Last().Minute);
                    p.RemoveAllChildren();
                    p.AppendChild(doc.CreateTextNode(temp));
                });
            //time in report set

            doc.DocumentNode.Descendants("p")
                .Where(p => p.HasClass("test-date"))
                .ForEach(p =>
                {
                    temp = p.InnerText;
                    temp = string.Format(temp, analysis.DateStarted);
                    p.RemoveAllChildren();
                    p.AppendChild(doc.CreateTextNode(temp));
                });
            
            var json = JsonConvert.SerializeObject(_analysis as Analysis);

            doc.DocumentNode.Descendants("input")
                .Single(d => d.Attributes.Contains("name") && d.Attributes["name"].Value.Contains("json-data"))
                .SetAttributeValue("value", json);

            json = JsonConvert.SerializeObject(_config as Configuration);

            doc.DocumentNode.Descendants("input")
                .Single(d => d.Attributes.Contains("name") && d.Attributes["name"].Value.Contains("json-config"))
                .SetAttributeValue("value", json);

            HtmlNode element;

            //table generator
            element = doc.DocumentNode.Descendants("table")
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains("results"));

            HtmlNode tempHtml;

            foreach (var result in _analysis.Results)
            {
                tempHtml = doc.CreateElement("th");
                tempHtml.AppendChild(doc.CreateTextNode($"{result.Minute} min"));

                element.Descendants("tr")
                    .Single(d =>
                        d.HasClass("column-headers"))
                    .AppendChild(tempHtml);

                tempHtml = doc.CreateElement("td");
                tempHtml.AppendChild(doc.CreateTextNode($"{result.H2Value}"));

                element.Descendants("tr")
                    .Single(d =>
                        d.HasClass("h2"))
                    .AppendChild(tempHtml);

                tempHtml = doc.CreateElement("td");
                tempHtml.AppendChild(doc.CreateTextNode($"{result.CH4Value}"));

                element.Descendants("tr")
                    .Single(d =>
                        d.HasClass("ch4"))
                    .AppendChild(tempHtml);

                tempHtml = doc.CreateElement("td");
                tempHtml.AppendChild(doc.CreateTextNode($"{result.H2Value + result.CH4Value}"));

                element.Descendants("tr")
                    .Single(d =>
                        d.HasClass("h2ch4"))
                    .AppendChild(tempHtml);

                tempHtml = doc.CreateElement("td");
                tempHtml.AppendChild(doc.CreateTextNode(@"<span class=""icon icon-ok""/>"));

                element.Descendants("tr")
                    .Single(d =>
                        d.HasClass("co2"))
                    .AppendChild(tempHtml);

                tempHtml = doc.CreateElement("td");
                tempHtml.AppendChild(doc.CreateTextNode($"{result.Time:HH:mm}"));

                element.Descendants("tr")
                    .Single(d =>
                        d.HasClass("hour"))
                    .AppendChild(tempHtml);
            }

            tempHtml = doc.CreateElement("td");
            tempHtml.AddClass("buffer");
            element.Descendants("tr")
                .Where(tr => !tr.HasClass("full-width"))
                .ForEach(tr => tr.AppendChild(tempHtml));

            var CHART_WIDTH = 754;
            var CHART_OFFSET_L = 105;
            var CHART_OFFSET_R = 105;

            var BASE_CELL_WIDTH = (float)CHART_WIDTH / analysis.Results.Count;
            var ROW_HEADER_WIDTH = (float)CHART_OFFSET_L - (BASE_CELL_WIDTH / 2);
            var BUFFER_WIDTH = (float)CHART_OFFSET_R - (BASE_CELL_WIDTH / 2);

            var backup = CultureInfo.CurrentCulture;
            CultureInfo.CurrentCulture = new CultureInfo("en-US");

            element.Descendants("td").ForEach(td =>
                {
                    td.SetAttributeValue("style", $"width:{100/analysis.Results.Count:F2}%;max-width:{100 / analysis.Results.Count:F2}%;");
                });
            element.Descendants("th").ForEach(td =>
            {
                td.SetAttributeValue("style", $"width:{100 / analysis.Results.Count:F2}%;max-width:{100 / analysis.Results.Count:F2}%;");
            });

            element.Descendants("th")
                .Where(th => th.HasClass("row-header"))
                .ForEach(th =>
            {
                    th.SetAttributeValue("style", $"width:{ROW_HEADER_WIDTH:F2}px;max-width:{ROW_HEADER_WIDTH:F2}px;min-width:{ROW_HEADER_WIDTH:F2}px;");
            });

            element.Descendants("td")
                .Where(td => td.HasClass("buffer"))
                .ForEach(td =>
                {
                    td.SetAttributeValue("style", $"width:{BUFFER_WIDTH:F2}px;max-width:{BUFFER_WIDTH:F2}px;min-width:{BUFFER_WIDTH:F2}px;");
                });

            if (_analysis.Results.Count > 18)
            {
                element.Descendants("tr")
                    .Where(tr => tr.HasClass("column-headers") || tr.HasClass("hour"))
                    .ForEach(tr =>
                    {
                        tr.AddClass("small");
                    });
            }

            int resultMax = (int)(new Collection<float> { _analysis.Results.Max(r => r.H2Value), _analysis.Results.Max(r => r.CH4Value), _config.H2Limit, _config.CH4Limit }.Max() + 20.0f);

            while (resultMax % 20 != 0) resultMax++;

            doc.DocumentNode.Descendants()
                .Single(input => input.Attributes.Contains("id") && input.Attributes["id"].Value.Equals("result-max"))
                .SetAttributeValue("value", $"{resultMax:F0}");

            CultureInfo.CurrentCulture = backup;
            //table generator end

            doc.DocumentNode.Descendants("input")
                .Single(d => d.Attributes.Contains("name") && d.Attributes["name"].Value.Contains("icon-ok"))
                .SetAttributeValue("value", Resources.icon_ok.Base64Encoded(ImageFormat.Png));
            doc.DocumentNode.Descendants("input")
                .Single(d => d.Attributes.Contains("name") && d.Attributes["name"].Value.Contains("icon-wrong"))
                .SetAttributeValue("value", Resources.icon_wrong.Base64Encoded(ImageFormat.Png));

            //conf-values

            //h2-base-conf
            element = doc.DocumentNode.Descendants()
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("h2-base-conf"));
            temp = string.Format(element.InnerText,_config.BaseH2Limit);
            element.RemoveAllChildren();
            element.AppendChild(doc.CreateTextNode(temp));

            //h2-diff-conf
            element = doc.DocumentNode.Descendants()
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("h2-diff-conf"));
            temp = string.Format(element.InnerText, _config.H2Limit);
            element.RemoveAllChildren();
            element.AppendChild(doc.CreateTextNode(temp));

            //ch4-peak-conf
            element = doc.DocumentNode.Descendants()
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("ch4-peak-conf"));
            temp = string.Format(element.InnerText, _config.CH4Limit);
            element.RemoveAllChildren();
            element.AppendChild(doc.CreateTextNode(temp));

            //h2ch4-diff-conf
            element = doc.DocumentNode.Descendants()
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("h2ch4-diff-conf"));
            temp = string.Format(element.InnerText, _config.CH4H2Limit);
            element.RemoveAllChildren();
            element.AppendChild(doc.CreateTextNode(temp));

            //analysis-values
            //h2-base
            element = doc.DocumentNode.Descendants()
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("h2-base"));
            temp = string.Format(element.InnerText, _report.BaseH2);
            element.RemoveAllChildren();
            element.AppendChild(doc.CreateTextNode(temp));
            if (_report.Results().Contains(Report.Result.TooHighH2Base))
                element.AddClass("wrong");
                

            //h2-diff
            element = doc.DocumentNode.Descendants()
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("h2-diff"));
            temp = string.Format(element.InnerText, _report.RangeH2);
            element.RemoveAllChildren();
            element.AppendChild(doc.CreateTextNode(temp));
            if (_report.Results().Contains(Report.Result.TooHighH2Range))
                element.AddClass("wrong");

            //ch4-peak
            element = doc.DocumentNode.Descendants()
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("ch4-peak"));
            temp = string.Format(element.InnerText, _report.PeakCh4);
            element.RemoveAllChildren();
            element.AppendChild(doc.CreateTextNode(temp));
            if (_report.Results().Contains(Report.Result.TooHighCh4Peak))
                element.AddClass("wrong");

            //h2ch4-diff
            element = doc.DocumentNode.Descendants()
                .Single(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("h2ch4-diff"));
            temp = string.Format(element.InnerText, _report.RangeH2Ch4);
            element.RemoveAllChildren();
            element.AppendChild(doc.CreateTextNode(temp));
            if (_report.Results().Contains(Report.Result.TooHighH2Ch4Range))
                element.AddClass("wrong");

            //return doc if test ok
            if (_report.Results().Contains(Report.Result.Ok))
                return doc.DocumentNode.OuterHtml;

            element = doc.DocumentNode.Descendants("div")
                .Single(div => div.HasClass("comments"));

            _report.Results().ForEach(r =>
            {
                string title = string.Empty;
                string content = string.Empty;

                switch (r)
                {
                    case Report.Result.TooHighH2Base:
                        title = @"Podwyższony poziom początkowy wodoru (H<sub>2</sub>)";
                        content = $"Wyjściowy wynik wodoru(H<sub>2</sub>) większy niż {_config.BaseH2Limit} ppm może wskazywać zarówno na obecność bakterii w jelicie cienkim, jak i na nieprawidłowe przygotowanie do testu! (Wówczas wynik należy interpretować indywidualnie.)";
                        break;
                    case Report.Result.TooHighH2Range:
                        title = @"Podwyższony poziom wodoru (H<sub>2</sub>)";
                        content = $"W ciągu pierwszych 120 minut** po spożyciu substratu wzrost ilości wodoru o więcej niż {_config.H2Limit} ppm lub więcej od najniższego poprzedniego wyniku, może wskazywać na przerost bakterii w jelicie cienkim.";
                        break;
                    case Report.Result.TooHighCh4Peak:
                        title = @"Podwyższony poziom metanu (CH<sub>4</sub>)";
                        content = $"W dowolnym pomiarze wynik metanu większy lub równy {_config.CH4Limit} ppm może sugerować nadmierny wzrost metanogenu.<br/>Badania wykazały, że istnieje związek pomiędzy przerostem metanowym a IBS jeśli u pacjenta dominują zaparcia.";
                        break;
                    case Report.Result.TooHighH2Ch4Range:
                        title = @"Podwyższona suma poziomów wodoru (H<sub>2</sub>) i metanu (CH<sub>4</sub>)";
                        content = $"W ciągu pierwszych 120 minut** po spożyciu substratu wzrost sumy wodoru i metanu o wartość większą, lub równą {_config.CH4H2Limit} ppm od najniższej wcześniejszej sumy, może wskazywać na przerost bakterii w jelicie cienkim.";
                        break;
                }

                tempHtml = doc.CreateElement("div");
                tempHtml.AddClass("container");

                var titleHtml = doc.CreateElement("h2");
                titleHtml.AppendChild(doc.CreateTextNode(title));

                var contentHtml = doc.CreateElement("p");
                contentHtml.AppendChild(doc.CreateTextNode(content));

                tempHtml.AppendChild(titleHtml);
                tempHtml.AppendChild(contentHtml);

                element.AppendChild(tempHtml);
            });

            return doc.DocumentNode.OuterHtml;
        }
    }
}