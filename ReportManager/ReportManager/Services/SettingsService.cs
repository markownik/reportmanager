﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using ReportManager.Helpers;
using ReportManager.Interface.Models;
using ReportManager.Interface.Services;
using ReportManager.Models;

namespace ReportManager.Services
{
    public class SettingsService : ISettingsService
    {
        public SettingsService()
        {
        }

        private Configuration Init()
        {
            var config = new Configuration();
            config.DefaultMinutes = new ObservableCollection<int> {0, 20, 40, 60, 80, 90, 100, 120, 140, 160, 180};
            Save(config);
            return config;
        }

        public IConfiguration Load()
        {
            if (!File.Exists(AppSettings.SettingsFilename))
            {
                return Init();
            }
            else
            {
                try
                {
                    return JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(AppSettings.SettingsFilename));
                }
                catch (Exception e)
                {
                    Debug.Write(e);
                    return Init();
                }
            }
        }

        public void Save(IConfiguration value)
        {
            File.WriteAllText(AppSettings.SettingsFilename, JsonConvert.SerializeObject(value as Configuration));
        }
    }
}