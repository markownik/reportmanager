﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using ReportManager.Helpers;
using ReportManager.Interface.Models;
using ReportManager.Interface.Services;
using ReportManager.Models;

namespace ReportManager.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IPdfService _pdfService;
        public IConfiguration Config { get; set; }

        public Analysis CurrentTest { get; set; }

        public ICommand ResetCommand => new DelegateCommand(Reset);

        public ICommand MakeReportCommand => new DelegateCommand(Generate);

        public ICommand AddResultCommand => new DelegateCommand(AddNewResult);

        public ICommand RemoveResultCommand => new DelegateCommand(RemoveResult);

        public MainViewModel(ISettingsService settingsService, IPdfService pdfService)
        {
            _pdfService = pdfService;
            if (Config == null)
            {
                Config = settingsService.Load();
                CurrentTest = new Analysis(Config);
            }
            else
            {
                if (CurrentTest == null)
                    CurrentTest = new Analysis(Config);    
            }
        }

        private void Reset()
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Czy chcesz wyczyścić wprowadzone dane?", "UWAGA", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.OK)
            {
                CurrentTest = new Analysis(Config);
            }
        }

        private void RemoveResult()
        {
            CurrentTest.RemoveSelected();
        }

        private void AddNewResult()
        {
            CurrentTest.AddNew();
        }

        private void Generate()
        {
            Task.Run(() =>
            {
                _pdfService.Init(Config);
                _pdfService.Load(CurrentTest);
            });

            bool positive = new Report(CurrentTest, Config).Positive;

            int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
            int dob = int.Parse(CurrentTest.Patient.Birthday.ToString("yyyyMMdd"));
            int age = (now - dob) / 10000;

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog
            {
                RestoreDirectory = true,
                InitialDirectory = AppSettings.FolderBase,
                FileName = $"Raport_SIBO_{CurrentTest.Patient.Surname}_{CurrentTest.Patient.Name}_{(age >= 18 ? "S" : "J")}_{(positive ? "P" : "N")}_{DateTime.Now:dd}_{DateTime.Now:MM}_{DateTime.Now:yyyy}",
                Filter = "Plik PDF (*.pdf)|*.pdf|Wszystkie pliki (*.*)|*.*",
                DefaultExt = "pdf"
            };
            
            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                Task.Run(() =>
                {
                    _pdfService.SaveToFile(filename);
                });
            }
        }
    }
}