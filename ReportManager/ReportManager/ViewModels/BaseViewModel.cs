﻿using Prism.Mvvm;
using Prism.Regions;

namespace ReportManager.ViewModels
{
    public abstract class BaseViewModel : BindableBase, INavigationAware
    {
        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return false;
        }

        public virtual void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public virtual void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }
    }
}