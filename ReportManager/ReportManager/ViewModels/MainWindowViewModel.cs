using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using ReportManager.Helpers;
using ReportManager.Interface.Services;
using ReportManager.Views;

namespace ReportManager.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        [DllImport("Kernel32")]
        public static extern void FreeConsole();

        private readonly IRegionManager _regionManager;
        private bool _consoleOn = false;

        public ICommand NavigateCommand => new DelegateCommand<string>(Navigate);

        public ICommand CloseWindowCommand => new DelegateCommand(Close);

        public ICommand ToggleConsoleCommand => new DelegateCommand(ToggleConsole);

        public MainWindowViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        private void Navigate(string uri)
        {
            _regionManager.RequestNavigate(AppSettings.MainWindowRegionName, uri);
        }

        private void Close()
        {
            Application.Current.MainWindow?.Close();
        }

        private void ToggleConsole()
        {
            if (_consoleOn)
            {
                ConsoleManager.Show();
            }
            else
            {
                ConsoleManager.Hide();
            }
            _consoleOn = !_consoleOn;
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Czy jeste� pewien(a)?", "Zamkni�cie aplikacji", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (messageBoxResult != MessageBoxResult.OK)
            {
               e.Cancel = true;
            }
        }
    }
}