﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;
using System.Windows.Navigation;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using ReportManager.Helpers;
using ReportManager.Interface.Models;
using ReportManager.Interface.Services;
using ReportManager.Models;
using ReportManager.Services;

namespace ReportManager.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        private readonly IRegionManager _regionManager;
        private readonly ISettingsService _settingsService;

        public ICommand CancelCommand => new DelegateCommand(Cancel);

        public ICommand SaveCommand => new DelegateCommand(Save);

        public Configuration Config { get; set; }

        public SettingsViewModel(IRegionManager regionManager, ISettingsService settingsService)
        {
            _regionManager = regionManager;
            _settingsService = settingsService;
        }

        private void Load()
        {
            Config = _settingsService.Load() as Configuration;
        }

        private void Save()
        {
            _settingsService.Save(Config);
            _regionManager.RequestNavigate(AppSettings.MainWindowRegionName, AppSettings.MainViewName);
        }

        private void Cancel()
        {
            _regionManager.RequestNavigate(AppSettings.MainWindowRegionName, AppSettings.MainViewName);
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Load();
        }
    }
}