﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ReportManager.Helpers
{
    public static class Extensions
    {
        public static string Base64Encoded(this Image image, ImageFormat format)
        {
            using (MemoryStream m = new MemoryStream())
            {
                image.Save(m, format);
                byte[] imageBytes = m.ToArray();

                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
    }
}