﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ReportManager.Views;

namespace ReportManager.Helpers
{
    public static class AppSettings
    {
        public static string AppTitle = "Program Obsługi Raportów";
        public static string MainWindowRegionName = "MainWindowContentRegion";
        public static string SettingsViewName = nameof(SettingsView);
        public static string MainViewName = nameof(MainView);
        public static string SettingsFilename = @"Settings.json";
        public static string TempFolderBase = $"{Path.GetTempPath()}ReportGenerator\\";
        public static string FolderBase = $"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\\";
        public static string TempFile = $"{Path.GetTempFileName()}_temp";
        public static string Wk32Path = "wkhtmltopdf\\win32\\wkhtmltopdf.exe";
        public static string Wk64Path = "wkhtmltopdf\\win64\\wkhtmltopdf.exe";
        public static bool Is64Bit = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"));
        public static string WkPath = $"{AppDomain.CurrentDomain.BaseDirectory}\\{(Is64Bit ? Wk64Path : Wk32Path)}";
        public static DpiScale DpiScale;
    }
}
