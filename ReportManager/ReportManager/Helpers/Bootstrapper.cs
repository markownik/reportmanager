﻿using System;
using System.Windows;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Unity;
using ReportManager.Interface.Services;
using ReportManager.Services;
using ReportManager.Views;
using Microsoft.Practices.Unity;

namespace ReportManager.Helpers
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            ViewModelLocationProvider.SetDefaultViewModelFactory(type => Container.Resolve(type));

            //Services

            Container.RegisterType<ISettingsService, SettingsService>();
            Container.RegisterType<IHtmlService, HtmlService>();
            Container.RegisterType<IPdfService, PdfService>();

            //Views

            Container.RegisterTypeForNavigation<MainView>();
            Container.RegisterTypeForNavigation<SettingsView>();
            
        }

        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow?.Show();

            /* dirty hax of regionManager not being able to navigate on viewmodel constructor */
            var regionManager = Container.Resolve<IRegionManager>();
            regionManager.RequestNavigate(AppSettings.MainWindowRegionName, AppSettings.MainViewName);
        }
    }
}
