﻿namespace ReportManager.Helpers
{
    using System;
    using Microsoft.Win32;

    public class DotNetVersion
    {
        public static Version Get()
        {
            var releaseKey = Get45PlusReleaseKey();
            
            if (releaseKey >= 461308)
                return new Version("4.7.1");
            if (releaseKey >= 460798)
                return new Version("4.7");
            if (releaseKey >= 394802)
                return new Version("4.6.2");
            if (releaseKey >= 394254)
                return new Version("4.6.1");
            if (releaseKey >= 393295)
                return new Version("4.6");
            if (releaseKey >= 379893)
                return new Version("4.5.2");
            if (releaseKey >= 378675)
                return new Version("4.5.1");
            if (releaseKey >= 378389)
                return new Version("4.5");

            return new Version("0.0.0");
        }

        private static int Get45PlusReleaseKey()
        {
            const string subkey = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\";

            using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32)
                .OpenSubKey(subkey))
            {
                if (ndpKey != null && ndpKey.GetValue("Release") != null)
                {
                    return (int) ndpKey.GetValue("Release");
                }
            }
            return -1;
        }

        // Checking the version using >= will enable forward compatibility.
        private static string CheckFor45PlusVersion(int releaseKey)
        {
            if (releaseKey >= 461308)
                return "4.7.1 or later";
            if (releaseKey >= 460798)
                return "4.7";
            if (releaseKey >= 394802)
                return "4.6.2";
            if (releaseKey >= 394254)
            {
                return "4.6.1";
            }
            if (releaseKey >= 393295)
            {
                return "4.6";
            }
            if ((releaseKey >= 379893))
            {
                return "4.5.2";
            }
            if ((releaseKey >= 378675))
            {
                return "4.5.1";
            }
            if ((releaseKey >= 378389))
            {
                return "4.5";
            }
            // This code should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return "No 4.5 or later version detected";
        }
    }
    // This example displays output like the following:
    //       .NET Framework Version: 4.6.1
}