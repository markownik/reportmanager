﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ReportManager.Interface.Models;

namespace ReportManager.Helpers
{
    public class Report
    {
        public readonly int BaseH2;
        public readonly int RangeH2;
        public readonly int PeakCh4;
        public readonly int RangeH2Ch4;
        public readonly bool Positive;

        private readonly ICollection<Result> _results;

        public enum Result
        {
            Ok,
            TooHighH2Base,
            TooHighH2Range,
            TooHighCh4Peak,
            TooHighH2Ch4Range
        }

        public Report(IAnalysis analysis, IConfiguration config)
        {
            _results = new Collection<Result>();
            Positive = true;

            BaseH2 = (int)analysis.Results[0].H2Value;

            var minutesRange = analysis.Results.OrderByDescending(r => r.H2Value).Where(r => r.Minute >= 90 && r.Minute <= 120);
            var maxPoint = minutesRange.First();

            RangeH2 = (int)(maxPoint.H2Value - BaseH2);

            PeakCh4 = (int)analysis.Results.Max(r => r.CH4Value);

            RangeH2Ch4 = (int)(RangeH2 + maxPoint.CH4Value);

            if (BaseH2 > config.BaseH2Limit)
                _results.Add(Result.TooHighH2Base);

            if (RangeH2 > config.H2Limit)
                _results.Add(Result.TooHighH2Range);

            if (PeakCh4 > config.CH4Limit)
                _results.Add(Result.TooHighCh4Peak);

            if (RangeH2Ch4 > config.CH4H2Limit)
                _results.Add(Result.TooHighH2Ch4Range);

            if (_results.Count == 0)
            {
                Positive = false;
                _results.Add(Result.Ok);
            }      
        }

        public ICollection<Result> Results()
        {
            return _results;
        }
    }
}