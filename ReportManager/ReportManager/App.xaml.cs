﻿using System;
using System.Diagnostics;
using System.Windows;
using ReportManager.Helpers;
using Microsoft.HockeyApp;

namespace ReportManager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            App.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

            Debug.WriteLine($".NET Framework {DotNetVersion.Get()} installed.");

            if (DotNetVersion.Get() < new Version("4.6.2"))
            {
                if (MessageBox.Show("Do prawidłowego działania program wymaga składnika \n.NET Framework (>= 4.6.2)",
                    @"UWAGA", 
                    MessageBoxButton.OK,
                    MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    App.Current.Shutdown(-2);
                }
            }

            HockeyClient.Current.Configure("b865ce0a9a9342679ed8ef38e097a430");

            var bs = new Bootstrapper();
            bs.Run();

            SetupHockeySDK();
        }

        private async void SetupHockeySDK()
        {
            await HockeyClient.Current.SendCrashesAsync(false);

            await HockeyClient.Current.CheckForUpdatesAsync(true, () =>
            {
                if (Application.Current.MainWindow != null) { Application.Current.MainWindow.Close(); }
                return true;
            });
        }
    }
}
