﻿using System;
using System.Windows;
using System.Windows.Media;
using Microsoft.Practices.ServiceLocation;
using ReportManager.Helpers;
using ReportManager.ViewModels;

namespace ReportManager.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            if (DataContext is MainWindowViewModel vm)
            {
                Closing += vm.OnWindowClosing;
            }
        }

        protected override void OnDpiChanged(DpiScale oldDpi, DpiScale newDpi)
        {
            base.OnDpiChanged(oldDpi, newDpi);
            AppSettings.DpiScale = newDpi;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            AppSettings.DpiScale = VisualTreeHelper.GetDpi(this);
        }
    }
}
