﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ReportManager.Interface.Models
{
    public interface IAnalysis
    {
        ObservableCollection<IResult> Results { get; set; }
        IPatient Patient { get; set; }
        IUser Supervisor { get; set; }
        DateTime DateStarted { get; set; }
    }
}