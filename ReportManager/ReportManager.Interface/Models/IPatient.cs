﻿using System;

namespace ReportManager.Interface.Models
{
    public interface IPatient
    {
        string Name { get; set; }
        string Surname { get; set; }
        string Email { get; set; }
        DateTime Birthday { get; set; }
    }
}