﻿using System.Collections.Generic;

namespace ReportManager.Interface.Models
{
    public interface IConfiguration
    {
        float BaseH2Limit { get; set; }
        float H2Limit { get; set; }
        float CH4Limit { get; set; }
        float CH4H2Limit { get; set; }
        ICollection<int> DefaultMinutes { get; set; }
    }
}