﻿using System;
using System.ComponentModel;

namespace ReportManager.Interface.Models
{
    public interface IResult : INotifyPropertyChanged
    {
        int Minute { get; set; }
        DateTime? Time { get; set; }
        DateTime? StartTime { get; set; }
        float H2Value { get; set; }
        float CH4Value { get; set; }
        bool CO2 { get; set; }
    }
}