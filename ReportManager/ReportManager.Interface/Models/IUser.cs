﻿namespace ReportManager.Interface.Models
{
    public interface IUser
    {
        string Name { get; set; }
        string Surname { get; set; }
    }
}