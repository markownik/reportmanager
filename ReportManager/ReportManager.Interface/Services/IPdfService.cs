﻿using ReportManager.Interface.Models;

namespace ReportManager.Interface.Services
{
    public interface IPdfService
    {
        void Init(IConfiguration config);
        void Load(IAnalysis analysis);
        void SaveToFile(string filename);
    }
}