﻿using ReportManager.Interface.Models;

namespace ReportManager.Interface.Services
{
    public interface ISettingsService
    {
        IConfiguration Load();
        void Save(IConfiguration value);
    }
}