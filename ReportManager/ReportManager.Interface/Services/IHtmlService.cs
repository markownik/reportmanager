﻿using ReportManager.Interface.Models;

namespace ReportManager.Interface.Services
{
    public interface IHtmlService
    {
        void Setup(IConfiguration configuration);
        string Generate(IAnalysis analysis);
    }
}